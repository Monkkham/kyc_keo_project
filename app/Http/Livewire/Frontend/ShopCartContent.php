<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Product;
use Livewire\Component;
use App\Models\ShopCart;

class ShopCartContent extends Component
{
    public $count_shop_cart,$qty;
    public function render()
    {
        $shop_cart = ShopCart::where('creator_id',auth()->user()->id)->get();
        $sum_subtotal = $shop_cart->sum('subtotal');
        $count_shop_carts = $shop_cart->count('id');
        return view('livewire.frontend.shop-cart-content',compact('shop_cart','sum_subtotal','count_shop_carts'))->layout('layouts.frontend.style');
    }
    public function Remove_Item($id)
    {
        $shop_cart = ShopCart::find($id);
        $shop_cart->delete();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon' => 'success',
        ]);
        return redirect(route('frontend.ShopCart'));
    }
    public function UpdateStock($id)
    {
        $shop_cart = ShopCart::find($id);
        $shop_cart->qty = $this->qty[$id];
        $shop_cart->subtotal = $shop_cart->price * $this->qty[$id];
        $shop_cart->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
    }
}
