<div class="col-lg-4 col-6 text-left">
    <form action="{{ route('frontend.Search') }}">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="ຄົ້ນຫາຂໍ້ມູນ..." name="search" wire:model="search" required>
            <div class="input-group-append">
                <button type="submit" class="input-group-text bg-transparent text-primary">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>
</div>
